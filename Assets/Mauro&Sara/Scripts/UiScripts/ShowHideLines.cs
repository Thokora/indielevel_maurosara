﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideLines : MonoBehaviour
{
    bool areVisibles = true;

    public void ToggleShowLines(bool show)
    {
        areVisibles = show;
        UiActions.ShowGuideLines(show);
    }
}
