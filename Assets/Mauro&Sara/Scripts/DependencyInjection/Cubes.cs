﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubes : ICubesManager
{
    /// <summary>
    /// Remember always put funtions as public
    /// </summary>
    private bool show = true;
    private bool CorrectOrder;
    private List<GameObject> AllLines = new List<GameObject>();
    private List<GameObject> CubesObjects = new List<GameObject>();
    private List<Vector3> CubesInitialPostions = new List<Vector3>();
    private List<Vector3> positionsToRandom = new List<Vector3>();
    private AudioSource soundfx;


    public void SetInitialPositions(List<GameObject> CubesList)
    {
        CubesObjects = CubesList;

        for (int i = 0; i < CubesList.Count; i++)
        {
            CubesInitialPostions.Add(CubesList[i].transform.position);
        }
    }

    public void BackToInitialPositions()
    {
        for (int i = 0; i < CubesObjects.Count; i++)
        {
            CubesObjects[i].transform.position = CubesInitialPostions[i];
        }
    }

    public void RandomizeCubesPositions(List<GameObject> randomPos, List<GameObject> cubesObject, int randomNumber)
    {
        positionsToRandom = new List<Vector3>();

        for (int i = 0; i < randomPos.Count; i++)
        {
            positionsToRandom.Add(randomPos[i].transform.position);
        }

        for (int i = 0; i < cubesObject.Count; i++)
        {
            randomNumber = Random.Range(0, positionsToRandom.Count);
            cubesObject[i].transform.position = positionsToRandom[randomNumber];
            positionsToRandom.RemoveAt(randomNumber);
        }

    }


    public void ShowControls(GameObject visualCtrlsFxPerCube, bool visible)
    {
        visualCtrlsFxPerCube.SetActive(visible);
    }

    public void Move(GameObject cubeObject, Vector3 direction, GameObject directionControls) //this function belongs to IPlatformWalkeable
    {
        Debug.Log("MoveCube called");

        Vector3 movement;

        Ray ray = new Ray(cubeObject.transform.position, direction);
        RaycastHit hit;

        Vector3 raySize = cubeObject.transform.position + (1f * direction);

        if (Physics.Raycast(ray, out hit, 1f))
        {
            if (hit.collider.gameObject.tag == "Cube")
            {
                Debug.Log("collider with cube or limit in direction: " + direction + " can't move");
            }
        }
        else
        {
            movement = cubeObject.transform.position + direction;

            if ((movement.x < 1.1f && movement.y < 1.1f && movement.z < 1.1f) && movement.x > -1.1f && movement.y > -1.1f && movement.z > -1.1f)
            {
                cubeObject.transform.position += direction;
            }
            else
            {
                directionControls.GetComponent<ParticleSystem>().Play();
                soundfx.Play();
            }
        }

        Debug.DrawLine(cubeObject.transform.position, raySize, Color.cyan);
    }

    public void SetSoundFx(AudioSource Sfx)
    {
        soundfx = Sfx;
    }

    public void CreateHelpLines(GameObject cubeObject, Vector3[] linePositions, Material[] lineMaterials)
    {
        LineRenderer lines = new GameObject().AddComponent<LineRenderer>();
        lines.useWorldSpace = false;
        lines.transform.SetParent(cubeObject.transform, false);
        lines.materials = lineMaterials;
        lines.startWidth = 0.15f;
        lines.endWidth = 0.15f;

        lines.positionCount = linePositions.Length;
    
        lines.SetPositions(linePositions);

        AllLines.Add(lines.gameObject);
    }

    public List<GameObject> GetAllLines ()
    {
        return AllLines;
    }
    
    public void CheckCorrectOrder(List<GameObject> CubesList, Material FireMat, Material IceMat, ICharactersManager characterManager)
    {
        int checker = 0;

        for (int i = 0; i < CubesList.Count; i++)
        {
            if (CubesList[i].transform.position == CubesInitialPostions[i])
            {
                checker++;
                CubesList[i].GetComponent<Renderer>().material = IceMat;
            }
            else
            {
                CubesList[i].GetComponent<Renderer>().material = FireMat;
                Debug.Log("Cube: " + checker + " has not Correct orden");
                CorrectOrder = false;
            }
        }

        if (checker == CubesList.Count)
        {
            Debug.Log("Every cube has Correct orden");
            CorrectOrder = true;
        }

        //Check if character girl can moves
        if (CubesList[0].transform.position == CubesInitialPostions[0])
        {
            bool girlChecker = true;
            int animNumber = 0;

            for (int i = 0; i < CubesList.Count; i++)
            {
                if (CubesList[i].transform.position == CubesInitialPostions[i])
                {
                    if (girlChecker)
                    {
                        animNumber++;
                        characterManager.AnimWalk(1,animNumber);
                    }
                }
                else
                {
                    girlChecker = false;
                }
            }
        }

        //Check if character boy can moves
        int maxNumber = CubesList.Count - 1;
        if (CubesList[maxNumber].transform.position == CubesInitialPostions[maxNumber])
        {
            bool boyChecker = true;
            int animNumber = 0;

            for (int i = maxNumber; i > 0; i--)
            {
                if (CubesList[i].transform.position == CubesInitialPostions[i])
                {
                    Debug.Log("Entra a al segundo chequeo de numero mayor");

                    if (boyChecker)
                    {

                        Debug.Log("Entra a la animacion");

                        animNumber++;
                        characterManager.AnimWalk(0, animNumber);
                    }
                }
                else
                {
                    boyChecker = false;
                }
            }
        }
    }

    public bool OrderCubesAreCorrect()
    {
        return CorrectOrder;
    }
}
