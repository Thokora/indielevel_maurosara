﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ControlsBehaviour : MonoBehaviour
{
    private ICubesManager _CubesManager;

    [SerializeField] private GameObject visualCtrlsFx;

    [SerializeField] [Tooltip("Controls: 0 = Top, 1= Bottom, 2 = Front, 3 = Back, 4 = Right, 5 = Left")]
    private GameObject[] directionControlsFx;

    [SerializeField] [Tooltip("Line points base on cube object")]
    private Vector3[] linepositions;

    [SerializeField]
    private Material[] lineMaterials;

    private Vector3 direction;

    private float rayDistance = 1f;

    [Inject]
    public void SetInject(ICubesManager cubesManager) // use this to inject dependencies, just add the the interface and name it
    {
        this._CubesManager = cubesManager;
    }

    private void Awake()
    {
        CreateGuideLines();
    }

    
    public void SelectedObject(bool visible)
    {
        _CubesManager.ShowControls(visualCtrlsFx, visible);
    }

    public void MoveXAxis(bool positiveDirection)
    {
        direction = Vector3.right;
        
        if (positiveDirection)
        {
            _CubesManager.Move(this.gameObject, direction, directionControlsFx[4]);
        }
        else
        {
            _CubesManager.Move(this.gameObject, -direction, directionControlsFx[5]);
        }
    }

    public void MoveYAxis(bool positiveDirection)
    {
        direction = Vector3.up;

        if (positiveDirection)
        {
            _CubesManager.Move(this.gameObject, direction, directionControlsFx[0]);
        }
        else
        {
            _CubesManager.Move(this.gameObject, -direction, directionControlsFx[1]);
        }
    }

    public void MoveZAxis(bool positiveDirection)
    {
        direction = Vector3.forward;

        if (positiveDirection)
        {
            _CubesManager.Move(this.gameObject, direction, directionControlsFx[3]);
        }
        else
        {
            _CubesManager.Move(this.gameObject, -direction, directionControlsFx[2]);
        }
    }

    public void CreateGuideLines()
    {
        _CubesManager.CreateHelpLines(this.gameObject, linepositions,lineMaterials);
    }

}
