﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICubesManager
{
    void SetInitialPositions(List<GameObject> CubesList);

    void BackToInitialPositions();

    void RandomizeCubesPositions(List<GameObject> randomPos, List<GameObject> cubesObject, int randomNumber);

    void Move(GameObject cubeObject, Vector3 direction, GameObject directionControls);

    void SetSoundFx(AudioSource Sfx);

    void ShowControls(GameObject visualCtrlsFxPerCube, bool visible);

    void CreateHelpLines(GameObject cubeObject, Vector3[] linePositions, Material[] lineMaterials);

    List<GameObject> GetAllLines();

    void CheckCorrectOrder(List<GameObject> CubesList, Material FireMat, Material IceMat, ICharactersManager characterManager);
    
    bool OrderCubesAreCorrect();
}
