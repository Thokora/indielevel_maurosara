﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Characters : ICharactersManager
{
    private Animator[] characterAnim;

    public void SetCharacters(GameObject[] Characters)
    {
        characterAnim = new Animator[Characters.Length];

        for (int i = 0; i < characterAnim.Length; i++)
        {
            characterAnim[i] = Characters[i].GetComponent<Animator>();
        }
    }

    public Animator[] CharactersAnimators()
    {
        return characterAnim;
    }

    public void AnimDefeated()
    {
        characterAnim[2].SetTrigger("Defeated");
        characterAnim[3].SetTrigger("Defeated");
    }

    public void AnimWalk(int idAnimator, int animNumber)
    {
        characterAnim[idAnimator].SetInteger("Walk",animNumber);

        characterAnim[idAnimator+2].SetTrigger("Walk");
    }

    public void AnimKiss()
    {
        characterAnim[2].SetTrigger("Kiss");
        characterAnim[3].SetTrigger("Kiss");
    }

    public void RestarAnims()
    {
        characterAnim[0].SetTrigger("ReStart");
        characterAnim[1].SetTrigger("ReStart");

        characterAnim[0].SetInteger("Walk", 0);
        characterAnim[1].SetInteger("Walk", 0);

        characterAnim[2].SetTrigger("Idle");
        characterAnim[3].SetTrigger("Idle");
    }

}
