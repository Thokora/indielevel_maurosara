﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TryAgain : MonoBehaviour
{
    public void ReStartScene()
    {
        UiActions.ReStart();
    }
    public void ReStartSceneSettings()
    {
        UiActions.ReStart();
        UiActions.ShowSettings(false);
        UiActions.ShowTutorial(false);
        UiActions.StartGame(true);
    }
}
