﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public static class UiActions
{
    public static Action <bool> StartGame;
    public static Action EndGame;
    public static Action <bool> ShowGuideLines;
    public static Action <bool> ShowTutorial;
    public static Action <bool> ShowSettings;
    public static Action<bool> ShowInvitation;
    public static Action Resolve;
    public static Action TryResolve;
    public static Action ReStart;
    public static Action <bool> ToggleMusic;
    public static Action<bool> ToggleSoundFx;
}
