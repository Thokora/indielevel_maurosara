using UnityEngine;
using Zenject;

public class DefaultInstaller : MonoInstaller
{
    public override void InstallBindings() // this creates instances of our classes
    {
        // every time creates a instance of IPlatformWakable converts to cubeManager as singleton
        Container.Bind<ICubesManager>().To<Cubes>().AsSingle();
        Container.Bind<ICharactersManager>().To<Characters>().AsSingle();
    }
}