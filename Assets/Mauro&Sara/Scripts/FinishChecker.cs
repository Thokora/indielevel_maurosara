﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FinishChecker : MonoBehaviour
{
    private ICharactersManager _CharactersManager;
    int playerInside = 0;

    [Inject]
    public void SetInjectCharacter(ICharactersManager charactersManager) // use this to inject dependencies, just add the the interface and name it
    {
        this._CharactersManager = charactersManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerInside++;
            if (playerInside == 2)
            {
                Debug.Log("entro 2");
                _CharactersManager.AnimKiss();
                StartCoroutine(WaitKissEnd());
                playerInside = 0;
            }
            else
            {
                other.gameObject.GetComponent<Animator>().SetTrigger("Idle");
            }
        }
    }

    IEnumerator WaitKissEnd()
    {
        yield return new WaitForSeconds(5f);
        UiActions.ShowInvitation(true);
    }
}
