﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    private GameObject CtrlCamera;
    [SerializeField]
    private Camera cam;
    [SerializeField]
    private Transform target;
    [SerializeField]
    private float zoomValue;
    [SerializeField]
    private float zoomSensibility;
    [SerializeField]
    private float minZoom;
    [SerializeField]
    private float maxZoom;

    private float targetZoom;
    private float distanceToTarget = -10;
    private bool AnimationCamera = false;
    private bool CanMoveCamera = false;

    private Vector3 previousPosition;

    private Vector3 eulerCam;

    private Vector3 positionCam;

    private void Awake()
    {
        UiActions.StartGame += StartGame;
        UiActions.EndGame += EndGame;
    }

    public void StartGame(bool playGame)
    {
        positionCam = new Vector3(cam.transform.position.x,cam.transform.position.y,cam.transform.position.z);
        eulerCam = new Vector3(cam.transform.eulerAngles.x, cam.transform.eulerAngles.y, cam.transform.eulerAngles.z);

        AnimationCamera = playGame;
        StarAnimationCamera();
    }

    void Update()
    {
        if (CanMoveCamera)
        {
            if (Input.GetMouseButtonDown(0))
            {
                previousPosition = cam.ScreenToViewportPoint(Input.mousePosition);
            }

            ZoomCamera();
            RotateCamera();
        }
    }

    void RotateCamera()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 newPosition = cam.ScreenToViewportPoint(Input.mousePosition);
            Vector3 direction = previousPosition - newPosition;

            float rotationAroundYAxis = -direction.x * 180; // camera moves horizontally
            float rotationAroundXAxis = direction.y * 180; // camera moves vertically

            cam.transform.position = target.position;

            cam.transform.Rotate(new Vector3(1, 0, 0), rotationAroundXAxis);
            cam.transform.Rotate(new Vector3(0, 1, 0), rotationAroundYAxis, Space.World); // <— This is what makes it work!
            
            cam.transform.Translate(new Vector3(0, 0.5f, distanceToTarget));

            previousPosition = newPosition;
        }
    }

    void ZoomCamera()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel");

        if (scroll < 0 || scroll > 0)
        {
            targetZoom -= scroll * zoomValue;
            targetZoom = Mathf.Clamp(targetZoom, minZoom, maxZoom);

            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, targetZoom, Time.deltaTime * zoomSensibility);
        }
    }

    void StarAnimationCamera()
    {
        if (AnimationCamera)
        {
            Debug.Log("camera animation");
            CanMoveCamera = false;
            StartCoroutine(WaitForAnimationEnd());
        }
        else
        {
            CanMoveCamera = false;
        }
    }
    IEnumerator WaitForAnimationEnd()
    {
        cam.transform.SetPositionAndRotation(positionCam,Quaternion.Euler(eulerCam));

        CtrlCamera.GetComponent<Animator>().SetTrigger("Start");
        yield return new WaitForSecondsRealtime(12f);
        CanMoveCamera = true;
    }

    void EndGame()
    {
        cam.transform.SetPositionAndRotation(positionCam, Quaternion.Euler(eulerCam));

        CanMoveCamera = false;
        CtrlCamera.GetComponent<Animator>().SetTrigger("End");
    }
}