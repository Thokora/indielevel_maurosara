﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICharactersManager
{
    void SetCharacters(GameObject[] Characters);

    Animator[] CharactersAnimators();

    void AnimDefeated();

    void AnimWalk(int idAnimator, int animNumber);
    
    void AnimKiss();

    void RestarAnims();
}
