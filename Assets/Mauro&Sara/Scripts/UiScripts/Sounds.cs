﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sounds : MonoBehaviour
{
    public void ToggleSfx(bool active)
    {
        UiActions.ToggleSoundFx(active);
    }

    public void ToggleMusic(bool active)
    {
        UiActions.ToggleMusic(active);
    }
}

