﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

sealed class GameManager : MonoBehaviour
{
    private ICubesManager _CubesManager;
    private ICharactersManager _CharactersManager;

    [SerializeField]
    [Tooltip("0 = CanvasTutorial, 1 = CanvasSettings, 2 = CanvasInvitation")]
    private GameObject[] AllCanvas;
    [SerializeField]
    private List<GameObject> RandomPos;
    [SerializeField]
    private List <GameObject> CubesObject;
    [SerializeField]
    private Material FireMat;
    [SerializeField]
    private Material IceMat;
    [SerializeField]
    private AudioSource music;
    [SerializeField]
    private AudioSource audioEffect;

    int walkValueBoy;
    int walkValueGirl;

    private int randomNumber;
    [SerializeField]
    [Tooltip("0 = CtrlBoy, 1 = CtrlGirl, 2 = Boy, 3 = Girl.")]
    private GameObject[] CtrlsAndCharacters;
    int scenePrepared = 0;
    List<GameObject> linePerCube = new List<GameObject>();

    [Inject]
    public void SetInjectCubes(ICubesManager cubesManager) // use this to inject dependencies, just add the the interface and name it
    {
        this._CubesManager = cubesManager;
    }
    [Inject]
    public void SetInjectCharacter(ICharactersManager charactersManager) // use this to inject dependencies, just add the the interface and name it
    {
        this._CharactersManager = charactersManager;
    }


    private void Awake()
    {
        _CubesManager.SetInitialPositions(CubesObject);
        _CubesManager.SetSoundFx(audioEffect);
        _CharactersManager.SetCharacters(CtrlsAndCharacters);
        UiActions.ShowTutorial += ShowCanvasTutorial;
        UiActions.ShowSettings += ShowCanvasSettings;
        UiActions.ShowInvitation += ShowCanvasInvitation;
        UiActions.ShowGuideLines += ShowLines;
        UiActions.TryResolve = TryResolve;
        UiActions.Resolve = Resolve;
        UiActions.ReStart = ReStartScene;
        UiActions.ToggleSoundFx = SFx;
        UiActions.ToggleMusic = Music;
    }

    private void Start()
    {
        UiActions.ShowTutorial(true);
    }
    
    public void ShowLines(bool show)
    {
        linePerCube = _CubesManager.GetAllLines();

        foreach (GameObject line in linePerCube)
        {
            line.SetActive(show);
        }
    }

    public void ShowCanvasTutorial(bool show)
    {
        AllCanvas[0].SetActive(show);
        if (!show)
        {
            StartCoroutine(PrepareScene());
        }
    }

    IEnumerator PrepareScene()
    {
        if (scenePrepared == 0)
        {
            _CubesManager.BackToInitialPositions();
            UiActions.ShowGuideLines(true);

            yield return new WaitForSecondsRealtime(13f);

            _CubesManager.RandomizeCubesPositions(RandomPos, CubesObject, randomNumber);
            scenePrepared = 1;
        }
    }

    public void ShowCanvasSettings(bool show)
    {
        AllCanvas[1].SetActive(show);
    }

    public void TryResolve()
    {
        _CubesManager.CheckCorrectOrder(CubesObject, FireMat, IceMat,_CharactersManager);

        if (_CubesManager.OrderCubesAreCorrect())
        {
            UiActions.EndGame();
        }
        else
        {
            walkValueBoy = _CharactersManager.CharactersAnimators()[0].GetInteger("Walk");
            walkValueGirl = _CharactersManager.CharactersAnimators()[1].GetInteger("Walk");

            StartCoroutine(WaitWalkEnd());
            
            //_CharactersManager.AnimDefeated(); // porner esto despues de que la animacion se detenga
        }
    }

    IEnumerator WaitWalkEnd()
    {
        if (walkValueBoy >= 1 && walkValueBoy < 4)
        {
            if (walkValueBoy == 1)
            {
                yield return new WaitForSeconds((float)walkValueBoy);
                _CharactersManager.CharactersAnimators()[2].SetTrigger("Idle");
            }
            else
            {
                yield return new WaitForSeconds((float)walkValueBoy + 1f);
                _CharactersManager.CharactersAnimators()[2].SetTrigger("Idle");
            }

        }
        if (walkValueGirl >= 1 && walkValueGirl < 4)
        {
            if (walkValueGirl == 1)
            {
                yield return new WaitForSeconds((float)walkValueGirl);
                _CharactersManager.CharactersAnimators()[3].SetTrigger("Idle");
            }
            else
            {
                yield return new WaitForSeconds((float)walkValueGirl + 1f);
                _CharactersManager.CharactersAnimators()[3].SetTrigger("Idle");
            }
        }
    }

    void ShowCanvasInvitation(bool show)
    {
        AllCanvas[2].SetActive(show);
    }

    public void Resolve()
    {
        _CubesManager.BackToInitialPositions();
        UiActions.TryResolve();
    }
    
    public void ReStartScene()
    {
        _CubesManager.RandomizeCubesPositions(RandomPos, CubesObject, randomNumber); //Make This After introduction
        _CubesManager.CheckCorrectOrder(CubesObject, FireMat, IceMat, _CharactersManager);
        

        _CharactersManager.RestarAnims();

        UiActions.ShowInvitation(false);

        _CubesManager.BackToInitialPositions();

        UiActions.ShowGuideLines(true);
        UiActions.ShowTutorial(true);
        scenePrepared = 0;
    }

    public void SFx(bool toggle)
    {
        audioEffect.mute = toggle;
    }

    public void Music(bool toggle)
    {
        music.mute = toggle;
    }
}
