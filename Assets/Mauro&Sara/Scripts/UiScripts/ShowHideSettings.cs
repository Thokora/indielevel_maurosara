﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideSettings : MonoBehaviour
{
    public void ShowSettings(bool isVisible)
    {
        UiActions.ShowSettings(isVisible);
    }

    public void Audio()
    {
        //Next
    }

    public void Music()
    {
        //Back
    }
    
    public void GuideLine()
    {
        //Back
    }

    public void Tutorial()
    {
        //Back
    }
}
