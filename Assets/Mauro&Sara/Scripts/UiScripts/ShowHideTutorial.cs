﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideTutorial : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Images;
    int idImage = 0;

    public void ShowTutorial(bool isVisible)
    {
        UiActions.ShowTutorial(isVisible);
        UiActions.StartGame(!isVisible);
    }

    public void Next()
    {
        Images[idImage].SetActive(false);
        idImage++;

        if (idImage < Images.Length)
        {
            Images[idImage].SetActive(true);
        }else
        {
            ShowTutorial(false);
            idImage = 0;
            Images[idImage].SetActive(true);
        }
        
    }

    public void Back()
    {
        if (idImage != 0)
        {
            Images[idImage].SetActive(false);
            idImage--;
            Images[idImage].SetActive(true);
        }
    }
}
